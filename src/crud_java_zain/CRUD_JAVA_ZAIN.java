/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud_java_zain;

import java.sql.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 *
 * @author zainal
 */
public class CRUD_JAVA_ZAIN {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_HOST = "jdbc:mysql://localhost/db_sekolah";
    static final String DB_USERNAME = "root";
    static final String DB_PASSWORD = "";

    static Connection _con;
    static Statement _state;
    static ResultSet _result;

    static InputStreamReader inputStreamReader = new InputStreamReader(System.in);
    static BufferedReader input = new BufferedReader(inputStreamReader);
    /**
     */
     public static void ShowMenu()
     {
            System.out.println("\n========= MAIN MENU =========");
            System.out.println("1. Insert Data");
            System.out.println("2. Show Data");
            System.out.println("3. Edit Data");
            System.out.println("4. Delete Data");
            System.out.println("0. Exit");
            System.out.println("");
            System.out.print("Choose> ");
            try {
        int pilihan = Integer.parseInt(input.readLine());
            switch (pilihan) {
                case 0:
                    System.exit(0);
                    break;
                case 1:
                    Store();
                    break;
                case 2:
                    showData();
                    break;
                case 3:
                    Update();
                    break;
                case 4:
                    Destroy();
                    break;
                default:
                    System.out.println("please choose!");
            }
            } catch (IOException | NumberFormatException e) {
                 System.out.println("error");
            }
     }

     
     public static void showData()
     {
         //set query sql
          String sql = "SELECT * FROM siswa";
            try {
                //assign to result data from execute query
                _result = _state.executeQuery(sql);

                System.out.println("+--------------------------------+");
                System.out.println("|     Data Siswa Kelas XIIRA     |");
                System.out.println("+--------------------------------+");
                while (_result.next()) {
                    //get result db
                    int id = _result.getInt("id");
                    int Nis = _result.getInt("Nis");
                    String Nama = _result.getString("Nama");
                    String Alamat = _result.getString("Alamat");
                    String Kelas = _result.getString("Kelas");
                    String Cita = _result.getString("Cita_cita");

                    System.out.println(String.format("%d. %s -- (%s)", id, Nis , Nama,Kelas, Alamat, Cita));
                }
            } catch (SQLException e) {
                System.out.println("error" + e);
            }
     }
     
     public static void Store()
     {
          try {
                // set user input data
                System.out.print("Nis : ");
                int Nis = Integer.parseInt(input.readLine());
                System.out.print("Nama : ");
                String Nama = input.readLine().trim();
                System.out.print("Kelas : ");
                String Kelas = input.readLine().trim();
                System.out.print("Cita: ");
                String Cita = input.readLine().trim();
                System.out.print("Alamat: ");
                String Alamat = input.readLine().trim();

                // set query params to db
                String sql = "INSERT INTO siswa (Nis, Nama,Kelas,Cita_cita,Alamat) VALUE('%s', '%s', '%s', '%s', '%s')";
                sql = String.format(sql,Nis,Nama,Kelas,Cita,Alamat);

                // store students to database
                _state.execute(sql);

            } catch (IOException | SQLException e) {
              System.out.println("error" + e);
            }
     }
     
     public static void Update()
     {
           try {  
                // set user input data & request params NIS
                System.out.print("id yang ingin di hapus: ");
                int id = Integer.parseInt(input.readLine());
                System.out.print("Nama : ");
                String Nama = input.readLine().trim();
                System.out.print("Kelas : ");
                String Kelas = input.readLine().trim();
                System.out.print("Cita: ");
                String Cita = input.readLine().trim();
                System.out.print("Alamat: ");
                String Alamat = input.readLine().trim();

                // query sql
                String sql = "UPDATE siswa SET Nama='%s',Kelas='%s',Cita_cita='%s',Alamat='%s' WHERE id=%d";
                sql = String.format(sql,Nama,Kelas,Cita,Alamat,id);

                // update date & save to database
                _state.execute(sql);

            } catch (IOException | NumberFormatException | SQLException e) {
                 System.out.println("error" + e);
            }
     }
     public static void Destroy()
     {
        try {
        
        // ambil input dari user
            System.out.print("id: ");
            int id = Integer.parseInt(input.readLine());

            // buat query hapus
            String sql = String.format("DELETE FROM siswa WHERE id=%d", id);
            // destroy data from database
            _state.execute(sql);

            System.out.println("Data telah terhapus...");
        } catch (IOException | NumberFormatException | SQLException e) {
               System.out.println("error" + e);
        } 
     }
    public static void main(String[] args) throws ClassNotFoundException {
     try {
        // register driver
            Class.forName(JDBC_DRIVER);

            _con = DriverManager.getConnection(DB_HOST, DB_USERNAME, DB_PASSWORD);
            _state = _con.createStatement();

            while (!_con.isClosed()) {
                ShowMenu();
            }

            _state.close();
            _con.close();

        } catch (SQLException e) {
            System.out.println("error" + e);
        }
    }
    
}
